# codingstyle.vim

This plugin is meant to help you respecting a specified coding style. Supported
styles:
* Linux coding style http://www.kernel.org/doc/Documentation/CodingStyle
* Kernel Normal Form https://en.wikipedia.org/wiki/Kernel_Normal_Form

## Installation

Copy and paste `pluging/ccodingstyle.vim` in your `.vim/plugin` directory. You can also
clone this repository to use with [Pathogen](https://github.com/tpope/vim-pathogen).

## Usage
The script defines the following commands:

`WhichCStyle` prints the current coding style.

`LinuxStyle` activates Linux coding style

`KNFStyle` activates Kernel Normal Form style

Rules based on the file's path can be specified for automatic setting of the
coding style. Add the following in your .vimrc

```{vim}
let g:ccstyle_linux_patterns = [ "/linux/", "/kernel/", "/path/to/your/linux/project" ]
let g:ccstyle_knf_patterns = [ "/path/to/your/knf/project" ]
```
When the first matching pattern in the edited file's path, its corresponding
coding style is activated. In case of a collision between patterns, KNF is set
by defaut.

## Credit
This script is inspired from the plugin written by Vivien Didelot:
 https://github.com/vivien/vim-linux-coding-style

## License

Copyright (c) Hamza Rihani. Distributed under the terms the Vim License. 
See `:help license.`
