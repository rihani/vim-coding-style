" Vim plugin to fit a selected coding style. Current implemented styles are
" Linux Conding Style and Kernel Normal Form (KNF) style.
" Maintainer:   Hamza Rihani <hamza.rihani@outlook.com>
" License:      Distributed under the same terms as Vim itself.
"
" This script is inspired from the plugin written by Vivien Didelot:
" https://github.com/vivien/vim-linux-coding-style
"
" For those who want to apply these options conditionally, you can define an
" array of patterns in your vimrc and these options will be applied only if
" the buffer's path matches one of the pattern. In the following example,
" options will be applied only if "/linux/" or "/kernel" is in buffer's path.
"
"   let g:ccstyle_linux_patterns = [ "/linux/", "/kernel/" ]
"   let g:ccstyle_knf_patterns = [ "/other/path/" ]

if exists("g:loaded_ccstyle")
    finish
endif

command! WhichCodingStyle call s:WhichCodingStyle()

let g:loaded_ccstyle = 1
let b:apply_linux_style = 0
let b:apply_knf_style = 0

set wildignore+=*.ko,*.mod.c,*.order,modules.builtin

augroup ccstyle
    autocmd!

    autocmd FileType c,cpp call s:CCSConfigure()
    autocmd FileType diff setlocal ts=8
    autocmd FileType kconfig setlocal ts=8 sw=8 sts=8 noet
    autocmd FileType dts setlocal ts=8 sw=8 sts=8 noet
augroup END

function s:CCSConfigure()

    let b:apply_linux_style = 0
    let b:apply_knf_style = 0

    if exists("g:ccstyle_linux_patterns")
        let path = expand('%:p')
        for p in g:ccstyle_linux_patterns
            if path =~ p
                let b:apply_linux_style = 1
                break
            endif
        endfor
    endif
    if exists("g:ccstyle_knf_patterns")
        let path = expand('%:p')
        for p in g:ccstyle_knf_patterns
            if path =~ p
                let b:apply_knf_style = 1
                break
            endif
        endfor
    endif

    if b:apply_linux_style
        call s:LinuxStyle()
    endif

    if b:apply_knf_style
        call s:KNFStyle()
    endif

endfunction

command! LinuxStyle call s:LinuxStyle()
command! KNFStyle call s:KNFStyle()

function! s:LinuxStyle()
    let b:apply_linux_style = 1
    let b:apply_knf_style = 0
    call s:LinuxFormatting()
    call s:CCSKeywords()
    call s:CCSHighlighting()
endfunction

function! s:KNFStyle()
    let b:apply_linux_style = 0
    let b:apply_knf_style = 1
    call s:KNFFormatting()
    call s:CCSKeywords()
    call s:CCSHighlighting()
endfunction

function s:LinuxFormatting()
    setlocal noexpandtab
    setlocal shiftwidth=8
    setlocal softtabstop=8
    setlocal tabstop=8
    setlocal textwidth=80

    setlocal cindent
    setlocal cinoptions=:0,l1,t0,g0,(0
endfunction

function s:KNFFormatting()
    setlocal noexpandtab
    setlocal shiftwidth=8
    setlocal softtabstop=0
    setlocal tabstop=8
    setlocal textwidth=80

    setlocal cindent
    setlocal cinoptions=:0,=8,l1,t0,(0.5s,m1
endfunction

function s:CCSKeywords()
    syn keyword cOperator likely unlikely
    syn keyword cType u8 u16 u32 u64 s8 s16 s32 s64
    syn keyword cType __u8 __u16 __u32 __u64 __s8 __s16 __s32 __s64
endfunction

function s:CCSHighlighting()
    highlight default link CCSError ErrorMsg

    syn match CCSError / \+\ze\t/     " spaces before tab
    syn match CCSError /\%>80v[^()\{\}\[\]<>]\+/ " virtual column 81 and more

    " Highlight trailing whitespace, unless we're in insert mode and the
    " cursor's placed right after the whitespace. This prevents us from having
    " to put up with whitespace being highlighted in the middle of typing
    " something
    autocmd InsertEnter * match CCSError /\s\+\%#\@<!$/
    autocmd InsertLeave * match CCSError /\s\+$/
endfunction

function s:WhichCodingStyle()
    if b:apply_knf_style
        echo "KNF Coding Style"
    elseif b:apply_linux_style
        echo "Linux Coding Style"
    else
        echo "Vim default C coding style"
    endif
endfunction

" vim: ts=4 et sw=4
